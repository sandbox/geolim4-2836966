<?php
namespace Drupal\phpfastcache\Cache;

use phpFastCache\Exceptions\phpFastCacheCoreException;

class UnsupportedMethodException extends phpFastCacheCoreException
{

}